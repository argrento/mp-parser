'''
Created on December 12, 2013

@author: Kirill Snezhko
'''

infoSections = ["[IMG ID]", "[Countries]", "[Regions]", "[Cities]",
                "[ZipCodes]", "[Restrict]"]
infoSectionsEnd = "[End-"

objectSections = ["[POI]", "[POLYGON]", "[POLYLINE]"]
objectProperties = ["Data0"]
objectSectionsEnd = "[END]"

inputFile = open("test_map.mp", "r")
outputFile = open("test_map_output.txt", "w")

linePrefix = "map_000000("
linePostfix = ")\n"

pntPrefix = ",pnt("
pntPostfix = ")"

def parseSection(outputFile, objectName, objectProperty):
    # Create line
    line = linePrefix
    
    # Replace brackets by quotes
    objectName = objectName.replace("]", "\"")
    objectName = objectName.replace("[", "\"")
    line += objectName
    
    # Keep only points
    objectProperty = objectProperty.split("=")[1]
    
    # Remove brackets
    objectProperty = objectProperty.replace(")", "")
    objectProperty = objectProperty.replace("(", "")
    
    # All coorinates -> to array
    points = objectProperty.split(",")
    
    # For every coordinate
    for i, point in enumerate(points):
        # if x, add to the line "pnt(" prefix
        if i % 2 == 0:
            line += pntPrefix + point + ","
        else:
            # if y, add postfix
            line += point + pntPostfix
    line += linePostfix
    outputFile.write(line)

objectName = ""
objectProperty = ""

#next(input).decode("cp1251").encode("utf-8")

for line in inputFile:
    # Find sections
    # rstrip() removes special symbols from line
    newLine = line.rstrip()

    if newLine in objectSections:
        objectName = newLine
    
    if line.find(objectProperties[0]) > -1:
        objectProperty = newLine
         
    if (objectProperty != "") and (objectName != ""):
        parseSection(outputFile, objectName, objectProperty)
        
    if line.find(objectSectionsEnd) > -1:
        objectName = ""
        objectProperty = ""
